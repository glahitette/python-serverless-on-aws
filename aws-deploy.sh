#!/usr/bin/env bash
set -e

# Function to perform a null check on multiple variables
nullCheck() {
    for var in "$@"; do
        if [ -z "$var" ]; then
            return 0  # Return 0 (true) if any variable is null or empty
        fi
    done
    return 1  # Return 1 (false) if all variables are not null or empty
}

# Check mandatory variables are defined (as project CI/CD variable)
# if nullCheck "$AWS_ACCESS_KEY_ID" "$AWS_SECRET_ACCESS_KEY" "$AWS_DEFAULT_REGION" "$AWS_SAM_BUCKET"; then
if nullCheck "$AWS_DEFAULT_REGION" "$AWS_SAM_BUCKET"; then
#    echo "At least one of the mandatory variables (AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_DEFAULT_REGION, AWS_SAM_BUCKET) is null or empty. Exiting."
    echo "At least one of the mandatory variables (AWS_DEFAULT_REGION, AWS_SAM_BUCKET) is null or empty. Exiting."
    exit 1
fi

echo "[aws-deploy] Deploy $environment_name..."

# disable AWS CLI pager
export AWS_PAGER=""

# try to create bucket if it doesn't exist yet
if ! aws s3 ls "$AWS_SAM_BUCKET"
then
  echo "SAM bucket ($AWS_SAM_BUCKET) not found: try to create..."
  aws s3api create-bucket --bucket "$AWS_SAM_BUCKET" --region "$AWS_DEFAULT_REGION" \
      --create-bucket-configuration LocationConstraint="$AWS_DEFAULT_REGION"
fi

# 1: build
sam build ${TRACE+--debug}

# 2: deploy (each environment is a separate stack)
sam deploy ${TRACE+--debug} \
  --stack-name "$environment_name" \
  --region "$AWS_DEFAULT_REGION" \
  --s3-bucket "$AWS_SAM_BUCKET" \
  --no-fail-on-empty-changeset \
  --no-confirm-changeset \
  --tags "ci-job-url=$CI_JOB_URL environment=$environment_type"

# Retrieve outputs (use cloudformation query)
api_url=$(aws cloudformation describe-stacks --stack-name "$environment_name" --output text --query 'Stacks[0].Outputs[?OutputKey==`AwsApiUrl`].OutputValue')

echo "Stack created/updated:"
echo " - Api URL: $api_url"

# Finally set the dynamically generated WebServer Url
echo "$api_url" > environment_url.txt

